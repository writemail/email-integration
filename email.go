package email

import (
	"fmt"
	"gopkg.in/mailgun/mailgun-go.v1"
	"log"
	"strings"
)

// Your available domain names can be found here:
// (https://app.mailgun.com/app/domains)
var yourDomain string = "writ.email" // e.g. mg.yourcompany.com

// The API Keys are found in your Account Menu, under "Settings":
// (https://app.mailgun.com/app/account/security)

// starts with "key-"
var privateAPIKey string = "key-036ee2f0b81b8a7d18f3c9931880cf95"

// starts with "pubkey-"
var publicValidationKey string = "pubkey-c84dd3270811dd559b050c43fb3b8e94"

// Create an instance of the Mailgun Client
var mg = mailgun.NewMailgun(yourDomain, privateAPIKey, publicValidationKey)

type Route struct {
	Priority    int      `json:"priority,omitempty"`
	Description string   `json:"description,omitempty"`
	Expression  string   `json:"expression,omitempty"`
	Actions     []string `json:"actions,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`
	ID        string `json:"id,omitempty"`
}

func SendMessage(sender, subject, body, recipient string) {
	message := mg.NewMessage(sender, subject, body, recipient)
	resp, id, err := mg.Send(message)

	if err != nil {
		log.Println(err)
	}

	fmt.Printf("Sender: %s Recipient: %s  Subject: %s ID: %s Resp: %s\n", sender, recipient, subject, id, resp)
}

func CreateRoute(routeName string) string {

	name := strings.ToLower(routeName)

	var actions []string
	actions = append(actions, "forward('https://writ.email/email/messages/received/"+name+"')")
	actions = append(actions, "stop()")

	route := mailgun.Route{
		Priority:    0,
		Description: "",
		Expression:  "match_recipient('" + name + "@writ.email')",
		Actions:     actions,
	}
	resp, err := mg.CreateRoute(route)

	if err != nil {
		log.Println(err)
	}

	return resp.ID
}
